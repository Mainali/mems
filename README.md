# README #

MEMS stands for Medical Equipment Management System.It is Java Desktop Application using Swing.

### What is this repository for? ###

* This is project for creating desktop app that keeps record of medical equipment, its supplier and its current status.The software is suppose to help the biomedical technicians or engineers working in hospitals,clinics,pathology labs in biomedical department to keep record and track status of the biomedical equipment being used in their working organization.
* Version 0.1


### How do I get set up? ###

* Pull the project or download zip
* This project uses mysql for database. So mysql server must be configured. The exported  sql database file is included in root of the project.
* All the dependencies are included in jar folder.
* Database configuration can be done in src/com/mems/utils/GlobalConnection.java file. And mail configuration can be done in src/com/mems/utils/MailHelper.java file.
* For now there are no test. i will include in later versions.
* Build the project, using ant or from ides(like netbeans). if build pass u can run the project.

### Snapshots ###

* [Home/Dashboard](https://bitbucket.org/Mainali/mems/raw/master/snapshots/1.png)
* [Record list](https://bitbucket.org/Mainali/mems/raw/master/snapshots/2.png)
* [Send Mail](https://bitbucket.org/Mainali/mems/raw/master/snapshots/3.png)
* [Edit form](https://bitbucket.org/Mainali/mems/raw/master/snapshots/4.png)

### Contribution guidelines ###

* contact me at mainalipukar@gmail.com.

### Who do I talk to? ###

* contact me at : mainalipukar@gmail.com
* Other community or team contact

