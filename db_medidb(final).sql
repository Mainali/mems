-- phpMyAdmin SQL Dump
-- version 4.1.6
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jan 22, 2016 at 12:35 PM
-- Server version: 5.6.16
-- PHP Version: 5.5.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `db_medidb`
--

-- --------------------------------------------------------

--
-- Table structure for table `departments`
--

CREATE TABLE IF NOT EXISTS `departments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `code_name` varchar(255) NOT NULL,
  `incharge` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `equipments`
--

CREATE TABLE IF NOT EXISTS `equipments` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `department` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `supplier` int(11) NOT NULL,
  `general_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `full_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `code` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `manufacturer` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `model_no` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `serial_no` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `installation_date` date NOT NULL,
  `warranty_type` enum('AMC','CMC','other','none') COLLATE utf8_unicode_ci NOT NULL,
  `category` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `cost` double NOT NULL,
  `warranty_start_date` date NOT NULL,
  `warranty_end_date` date NOT NULL,
  `notified` tinyint(1) NOT NULL DEFAULT '0',
  `created_by` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `updated_by` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=12 ;

--
-- Dumping data for table `equipments`
--

INSERT INTO `equipments` (`id`, `department`, `supplier`, `general_name`, `full_name`, `code`, `manufacturer`, `model_no`, `serial_no`, `installation_date`, `warranty_type`, `category`, `cost`, `warranty_start_date`, `warranty_end_date`, `notified`, `created_by`, `updated_by`, `created_at`, `updated_at`) VALUES
(1, '1', 1, 'Baby warmer123', 'Lullaby', 'OM20', 'GE Healthcare', 'LBY23', 'b20WHJ', '2015-06-10', 'AMC', 'life_care', 600000, '2015-11-30', '2015-11-30', 0, '', 'Mainali', '2015-06-10 08:52:15', '2016-01-17 18:15:00'),
(3, '1', 3, 'Baby Incubator', 'Lullaby', 'OM23', 'GE Healthcare', 'LBY256', 'lb7678', '2015-06-11', 'AMC', 'life_care', 600000, '2015-06-11', '2016-06-15', 0, '', '', '2015-06-10 20:26:54', '2015-06-10 20:26:54'),
(6, 'micu', 5, 'New', 'new Eq', 'MNSD2', 'GE', 'sdf324', '123asd', '2016-01-12', 'AMC', 'critical', 3241241, '2016-01-18', '2016-01-22', 0, 'Mainali', 'Mainali', '2016-01-17 18:15:00', '2016-01-18 18:15:00'),
(7, 'sfdsfsd', 4, 'Another', 'another', 'asfsd', 'dsfsad', 'dssdfsd', 'dsfasdf', '2016-01-11', 'AMC', 'sdaffa', 1235235, '2016-01-18', '0201-01-18', 0, 'Mainali', 'Mainali', '2016-01-17 18:15:00', '2016-01-17 18:15:00'),
(8, 'micu', 2, 'Name', 'Name-Full', 'sdaf3432', 'Ge', 'MOd3423', '325df', '2016-01-18', 'AMC', 'new', 3251234, '2016-01-18', '2016-01-18', 0, 'Mainali', 'Mainali', '2016-01-17 18:15:00', '2016-01-17 18:15:00'),
(9, 'micu', 1, 'Name-MAN', 'Name-Full', 'sdaf3432', 'Ge HealthCare', 'MOd3423', '325dfsdf', '2016-01-18', 'AMC', 'new', 3251234, '2016-01-18', '2016-01-18', 0, 'Mainali', 'Mainali', '2016-01-17 18:15:00', '2016-01-17 18:15:00'),
(10, 'icu', 2, 'naya', 'mane', '332', 'ge', 'sdfgs', 'ssdf434', '2016-01-21', 'AMC', 'sdfsdfsd', 24211, '2016-01-21', '2016-01-21', 0, 'Mainali', 'Mainali', '2016-01-21 18:15:00', '2016-01-21 18:15:00'),
(11, 'fgffdsdg', 2, 'some', 'nammenr', 'dsgf', 'qdfsgfdsg', 'dfgsdfg', '435432', '2016-01-12', 'AMC', 'sdfsdaf', 3245252, '2016-01-19', '2016-01-20', 0, 'pukar', 'pukar', '2016-01-21 18:15:00', '2016-01-21 18:15:00');

--
-- Triggers `equipments`
--
DROP TRIGGER IF EXISTS `equipment_insert_trigger`;
DELIMITER //
CREATE TRIGGER `equipment_insert_trigger` BEFORE INSERT ON `equipments`
 FOR EACH ROW insert into logs(date,changed_table,action,done_by) values(new.created_at,'equipments','insert',new.created_by)
//
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `logs`
--

CREATE TABLE IF NOT EXISTS `logs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `changed_table` varchar(255) NOT NULL,
  `action` varchar(255) NOT NULL,
  `done_by` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `service_logs`
--

CREATE TABLE IF NOT EXISTS `service_logs` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `service_num` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `date` date NOT NULL,
  `equipment_id` int(11) NOT NULL,
  `complaint` text COLLATE utf8_unicode_ci NOT NULL,
  `action_taken` text COLLATE utf8_unicode_ci NOT NULL,
  `service_person` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `call_type` enum('technical','operational','preventive','others') COLLATE utf8_unicode_ci NOT NULL,
  `part_changed` enum('no','yes') COLLATE utf8_unicode_ci NOT NULL,
  `breakdown_time` datetime NOT NULL,
  `completion_time` datetime NOT NULL,
  `remarks` text COLLATE utf8_unicode_ci NOT NULL,
  `updated_by` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_by` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=5 ;

--
-- Dumping data for table `service_logs`
--

INSERT INTO `service_logs` (`id`, `service_num`, `date`, `equipment_id`, `complaint`, `action_taken`, `service_person`, `call_type`, `part_changed`, `breakdown_time`, `completion_time`, `remarks`, `updated_by`, `created_by`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, '23432', '2015-06-11', 3, 'fgjfjgh', 'guog', 'subash and sushant bdr', 'technical', 'no', '2015-06-11 09:31:00', '2015-06-11 11:32:00', 'kjghkjg done..', '', '', NULL, '2015-06-10 22:03:27', '2015-06-15 06:07:38'),
(2, '55665', '2015-09-27', 3, 'checkout failed', 'circuit connected properly and re run the checkout and it passed.', 'pukar', 'operational', 'no', '2015-09-27 10:08:00', '2015-09-27 10:20:00', 'done', '', '', NULL, '2015-09-27 11:39:14', '2015-09-27 11:39:14'),
(3, '4352', '2015-06-11', 1, 'dsfsdaf', 'sdfsdaf', 'sdf', 'technical', 'no', '2015-06-11 04:07:23', '2015-06-11 08:07:23', 'dsfdsf', 'dsf', 'sdf', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(4, '2345', '2016-01-21', 6, 'heating issue', 'cleaning done', 'pukar', 'preventive', 'no', '2016-01-21 00:00:00', '2016-01-21 00:00:00', 'done', 'Mainali', 'Mainali', NULL, '2016-01-20 18:15:00', '2016-01-20 18:15:00');

--
-- Triggers `service_logs`
--
DROP TRIGGER IF EXISTS `service_log_insert_trigger`;
DELIMITER //
CREATE TRIGGER `service_log_insert_trigger` BEFORE INSERT ON `service_logs`
 FOR EACH ROW insert into logs(date,changed_table,action,done_by) values(new.created_at,'service_logs','insert',new.created_by)
//
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `suppliers`
--

CREATE TABLE IF NOT EXISTS `suppliers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `address` varchar(255) NOT NULL,
  `phone_no` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `suppliers`
--

INSERT INTO `suppliers` (`id`, `name`, `email`, `address`, `phone_no`) VALUES
(1, 'Sigma', 'sigma@gmail.com', 'thapathali', '54322435'),
(2, 'smarika', 'smarika@gmail.com', 'thapathali', '1234254'),
(3, 'Sigma', 'sigma@gmail.com', 'thapathali', '54322435'),
(4, 'smarika', 'smarika@gmail.com', 'thapathali', '1234254'),
(5, 'hospitec', 'jhj@fd.fj', 'gfgfd', '77858');

-- --------------------------------------------------------

--
-- Table structure for table `system_configs`
--

CREATE TABLE IF NOT EXISTS `system_configs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `organization` varchar(255) NOT NULL,
  `address` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `mail_smtp_host` varchar(255) NOT NULL,
  `mail_smtp_port` int(6) NOT NULL,
  `mail_auth_code` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `system_configs`
--

INSERT INTO `system_configs` (`id`, `organization`, `address`, `email`, `mail_smtp_host`, `mail_smtp_port`, `mail_auth_code`) VALUES
(1, 'fsdfs', 'sdfsd', 'mainalipukar@gmail.com', 'smtp.mandrillapp.com', 587, 'IGVG2LAEBGLano97f34QBw');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `role` enum('Guest','Admin') NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `username`, `password`, `email`, `role`) VALUES
(3, 'pukar', 'pukar123', 'mainalipukar@gmail.com', 'Admin'),
(4, 'pukk', 'pukar123', 'pukar@ekbana.com', 'Guest');

-- --------------------------------------------------------

--
-- Table structure for table `work_logs`
--

CREATE TABLE IF NOT EXISTS `work_logs` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `date` date NOT NULL,
  `work_done` text COLLATE utf8_unicode_ci NOT NULL,
  `in_time` time NOT NULL,
  `out_time` time NOT NULL,
  `remarks` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=8 ;

--
-- Dumping data for table `work_logs`
--

INSERT INTO `work_logs` (`id`, `user`, `date`, `work_done`, `in_time`, `out_time`, `remarks`, `created_at`, `updated_at`) VALUES
(1, '1', '2015-06-09', 'some critical work..', '07:00:00', '16:00:00', 'well done', '2015-06-08 22:49:15', '2015-06-08 23:35:58'),
(2, '2', '2015-06-09', 'different', '09:00:00', '14:00:00', 'ok', '2016-01-11 18:15:00', '2015-06-11 11:44:03'),
(3, '0', '2015-06-13', 'visited om hospital and done PM of 6 ventilators.', '10:05:00', '16:00:00', 'PM was passed successfully.', '2015-06-13 06:26:16', '2015-06-13 06:26:17'),
(4, '0', '2015-06-13', 'Tread mill issue was solved at Norvic Hospital.', '10:06:00', '18:07:00', 'done.', '2015-06-13 11:38:38', '2015-06-13 11:38:38'),
(5, '0', '2015-07-01', 'good work done', '10:00:00', '16:11:00', 'ok', '2015-07-01 04:41:43', '2015-07-01 04:41:43'),
(6, '0', '2015-07-19', 'updated eloquent query according to new laravel version 5.1', '21:20:00', '21:20:00', 'list()->toArray(); done.', '2015-07-19 09:52:38', '2015-07-19 09:52:38'),
(7, '2', '2016-01-09', 'Came to norvic to discuss about agro project', '10:20:00', '10:23:00', 'jhjh', '2015-11-30 22:54:12', '2015-11-30 22:54:12');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
