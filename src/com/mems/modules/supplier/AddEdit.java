/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mems.modules.supplier;

import com.mems.utils.DbHelper;
import static com.sun.org.apache.xalan.internal.lib.ExsltDatetime.date;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;

/**
 *
 * @author Pukar
 */
public final class AddEdit extends javax.swing.JPanel {


    /**
     * Creates new form AddEdit
     * @param frminfo
     */
    public AddEdit(Map frminfo){
        initComponents();
        this.forminfo = frminfo;
        this.modalObject= new DbHelper("suppliers");
        try {
            initForm();
        } catch (ParseException ex) {
            Logger.getLogger(AddEdit.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    Map<String, String> forminfo = new HashMap<>();
    
    DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
    
    Date date = new Date();
    
     private final DbHelper modalObject;
     
    public void initForm() throws ParseException
    {
        
        if("EditForm".equals(this.forminfo.get("formType")))
        {
        lblHeader.setText(" "+this.forminfo.get("name")+" "+this.forminfo.get("headerText"));
        name.setText(this.forminfo.get("name"));
        address.setText(this.forminfo.get("address"));
        phone_no.setText(this.forminfo.get("phone_no"));
        email.setText(this.forminfo.get("email"));
        }
        else
        {
            lblHeader.setText(this.forminfo.get("headerText"));
        }
       
       
    }
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        lblFormHeader = new javax.swing.JLabel();
        jPanel1 = new javax.swing.JPanel();
        name = new javax.swing.JTextField();
        email = new javax.swing.JTextField();
        address = new javax.swing.JTextField();
        jLabel4 = new javax.swing.JLabel();
        phone_no = new javax.swing.JTextField();
        jLabel3 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel1 = new javax.swing.JLabel();
        btnSubmit = new javax.swing.JButton();
        lblHeader = new javax.swing.JLabel();

        setLayout(null);

        lblFormHeader.setFont(new java.awt.Font("Times New Roman", 1, 24)); // NOI18N
        lblFormHeader.setText("Equipment");
        add(lblFormHeader);
        lblFormHeader.setBounds(68, 11, 113, 28);

        name.setName(""); // NOI18N
        name.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                nameActionPerformed(evt);
            }
        });

        jLabel4.setText("Email");

        jLabel3.setText("Phone No");

        jLabel2.setText("Address");

        jLabel1.setText("Supplier Name");

        btnSubmit.setFont(new java.awt.Font("Times New Roman", 1, 12)); // NOI18N
        btnSubmit.setText("Submit");
        btnSubmit.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSubmitActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(57, 57, 57)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(btnSubmit)
                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                        .addGroup(jPanel1Layout.createSequentialGroup()
                            .addComponent(jLabel2)
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(address, javax.swing.GroupLayout.PREFERRED_SIZE, 163, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGroup(jPanel1Layout.createSequentialGroup()
                            .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                .addComponent(jLabel1)
                                .addComponent(jLabel3, javax.swing.GroupLayout.Alignment.LEADING)
                                .addComponent(jLabel4, javax.swing.GroupLayout.Alignment.LEADING))
                            .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addGroup(jPanel1Layout.createSequentialGroup()
                                    .addGap(83, 83, 83)
                                    .addComponent(name, javax.swing.GroupLayout.PREFERRED_SIZE, 163, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addComponent(phone_no, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 163, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(email, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 163, javax.swing.GroupLayout.PREFERRED_SIZE)))))))
                .addContainerGap(290, Short.MAX_VALUE))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1)
                    .addComponent(name, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel2)
                    .addComponent(address, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel3)
                    .addComponent(phone_no, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel4)
                    .addComponent(email, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addComponent(btnSubmit, javax.swing.GroupLayout.PREFERRED_SIZE, 36, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(132, Short.MAX_VALUE))
        );

        add(jPanel1);
        jPanel1.setBounds(71, 92, 661, 295);

        lblHeader.setFont(new java.awt.Font("Times New Roman", 1, 24)); // NOI18N
        lblHeader.setText("jLabel14");
        add(lblHeader);
        lblHeader.setBounds(187, 11, 545, 28);
    }// </editor-fold>//GEN-END:initComponents

    private void btnSubmitActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSubmitActionPerformed
        // TODO add your handling code here:
        
        //String date = df.format( warranty_start_date.getDate());
//        String nameText = supplier_name.getText();
//        String addressText = address.getText();
//        String phoneText = phone_no.getText();
//        String emailText = email.getText();
        if(name.getText().isEmpty()||address.getText().isEmpty()||phone_no.getText().isEmpty()||email.getText().isEmpty())
        {
           
            JOptionPane.showMessageDialog(null, "Required * fields cannot be empty.");
        }
        else
        {
            String msg="";int err=1;
          Map<String, String> forminputs = new HashMap<>();

            forminputs.put("name", name.getText());
            forminputs.put("address", address.getText());
            forminputs.put("phone_no", phone_no.getText());
            forminputs.put("email", email.getText());

            if(this.forminfo.get("id")== null)
            {
                        if(modalObject.selectCount("name='"+name.getText()+"'") > 0)
                        {
                            msg +="Supplier name already exists.";
                            err =1;
                        }
                        
                  if(err!=1)
                  {
                      forminputs.put("created_by",forminfo.get("user_name"));
                    forminputs.put("created_at",df.format(date));
                    forminputs.put("updated_by",forminfo.get("user_name"));
                    forminputs.put("updated_at",df.format(date));
                    this.modalObject.insertData(forminputs);

                    JOptionPane.showMessageDialog(null, "Successfully Added Supplier");
                  }
                  else
                  {
                    JOptionPane.showMessageDialog(null, msg);  
                  }
             
            }
            else
            {
                if(modalObject.selectCount("name='"+name.getText()+"'AND id<>'"+this.forminfo.get("id")+"'") > 0)
                        {
                            msg +="Supplier name already exists.";
                            err =1;
                        }
                if(err!=1)
                {
                    forminputs.put("updated_by",forminfo.get("user_name"));
                    forminputs.put("updated_at",df.format(date));
                  String id = this.forminfo.get("id");
                  this.modalObject.updateData(forminputs, id);
                  JOptionPane.showMessageDialog(null, "Successfully Updated"+name.getText());
                }
                else
                  {
                    JOptionPane.showMessageDialog(null, msg);  
                  }
                
            }
            
        }
        
        
       
        //this.exit();
    }//GEN-LAST:event_btnSubmitActionPerformed

    private void nameActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_nameActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_nameActionPerformed
   


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JTextField address;
    private javax.swing.JButton btnSubmit;
    private javax.swing.JTextField email;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JLabel lblFormHeader;
    private javax.swing.JLabel lblHeader;
    private javax.swing.JTextField name;
    private javax.swing.JTextField phone_no;
    // End of variables declaration//GEN-END:variables
}
