/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mems.modules.servicelogs;

import com.mems.utils.DbHelper;
import com.mems.utils.ValidationHelper;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;

/**
 *
 * @author Pukar
 */
public final class AddEdit extends javax.swing.JPanel {

    /**
     * Creates new form AddEdit
     * @param frminfo
     */
    public AddEdit(Map frminfo){
        initComponents();
        this.forminfo = frminfo;
        this.modalObject = new DbHelper("service_logs");
        try {
            initForm();
        } catch (ParseException ex) {
            Logger.getLogger(AddEdit.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    Map<String, String> forminfo = new HashMap<>();
    
    DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
    
    Date date = new Date();
    
     private final DbHelper modalObject;
     
     DbHelper modalEquipmentsObject = new DbHelper("equipments");
     
    public void initForm() throws ParseException
    {
        call_type.addItem("");
       call_type.addItem("operational");
       call_type.addItem("technical");
       call_type.addItem("preventive");
       
       part_changed.addItem("");
       part_changed.addItem("yes");
       part_changed.addItem("no");
      
        
        Map<String, String> listEquipments = modalEquipmentsObject.selectListMapped("serial_no,id");
        equipment.addItem("");
        for (Object val : listEquipments.values()) {
            equipment.addItem(val.toString());
        }
        //supplier.addItem("smarika");
        
        
        if("EditForm".equals(this.forminfo.get("formType")))
        {
           lblHeader.setText(" "+this.forminfo.get("service_num")+" "+this.forminfo.get("headerText"));
           equipment.setSelectedItem(listEquipments.get(this.forminfo.get("equipment_id")));
       
        service_num.setText(this.forminfo.get("service_num"));
        dateText.setDate(df.parse(this.forminfo.get("date")));
        //equipment.setSelectedItem(this.forminfo.get("equipment"));
        complaintText.setText(this.forminfo.get("complaint"));
        action_taken.setText(this.forminfo.get("action_taken"));
        service_person.setText(this.forminfo.get("service_person"));
        call_type.setSelectedItem(this.forminfo.get("call_type"));
        part_changed.setSelectedItem(this.forminfo.get("part_changed"));   
        
        breakdown_time.setDate(df.parse(this.forminfo.get("breakdown_time"))); 
        completion_time.setDate(df.parse(this.forminfo.get("completion_time"))); 
        remarks.setText(this.forminfo.get("remarks"));
        }
        else
        {
            lblHeader.setText(this.forminfo.get("headerText"));
        }
       
       
    }
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jScrollPane1 = new javax.swing.JScrollPane();
        complaint = new javax.swing.JTextArea();
        basicDatePickerUI1 = new org.jdesktop.swingx.plaf.basic.BasicDatePickerUI();
        basicDatePickerUI2 = new org.jdesktop.swingx.plaf.basic.BasicDatePickerUI();
        lblFormHeader = new javax.swing.JLabel();
        jPanel1 = new javax.swing.JPanel();
        service_num = new javax.swing.JTextField();
        jLabel1 = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        dateText = new org.jdesktop.swingx.JXDatePicker();
        jLabel9 = new javax.swing.JLabel();
        equipment = new org.jdesktop.swingx.JXComboBox();
        jLabel3 = new javax.swing.JLabel();
        jScrollPane2 = new javax.swing.JScrollPane();
        complaintText = new javax.swing.JTextArea();
        jLabel4 = new javax.swing.JLabel();
        service_person = new javax.swing.JTextField();
        jLabel5 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        breakdown_time = new org.jdesktop.swingx.JXDatePicker();
        jLabel10 = new javax.swing.JLabel();
        completion_time = new org.jdesktop.swingx.JXDatePicker();
        jLabel11 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jScrollPane3 = new javax.swing.JScrollPane();
        action_taken = new javax.swing.JTextArea();
        jLabel12 = new javax.swing.JLabel();
        jScrollPane4 = new javax.swing.JScrollPane();
        remarks = new javax.swing.JTextArea();
        btnSubmit = new javax.swing.JButton();
        call_type = new org.jdesktop.swingx.JXComboBox();
        part_changed = new org.jdesktop.swingx.JXComboBox();
        lblHeader = new javax.swing.JLabel();

        complaint.setColumns(20);
        complaint.setRows(5);
        jScrollPane1.setViewportView(complaint);

        setLayout(null);

        lblFormHeader.setFont(new java.awt.Font("Times New Roman", 1, 24)); // NOI18N
        lblFormHeader.setText("Service Log");
        add(lblFormHeader);
        lblFormHeader.setBounds(68, 11, 121, 28);

        jLabel1.setText("Service No");

        jLabel8.setText("Date");

        jLabel9.setText("Equipment");

        complaintText.setColumns(20);
        complaintText.setRows(5);
        jScrollPane2.setViewportView(complaintText);

        jLabel4.setText("Service Person");

        service_person.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                service_personActionPerformed(evt);
            }
        });

        jLabel5.setText("Call Type");

        jLabel6.setText("Part Changed");

        jLabel7.setText("Break Down Time");

        jLabel10.setText("Completion Time");

        jLabel11.setText("Complaint");

        jLabel2.setText("Action Taken");

        action_taken.setColumns(20);
        action_taken.setRows(5);
        jScrollPane3.setViewportView(action_taken);

        jLabel12.setText("Remarks");

        remarks.setColumns(20);
        remarks.setRows(5);
        jScrollPane4.setViewportView(remarks);

        btnSubmit.setFont(new java.awt.Font("Times New Roman", 1, 12)); // NOI18N
        btnSubmit.setText("Submit");
        btnSubmit.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSubmitActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel9)
                    .addComponent(jLabel8)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                .addGroup(jPanel1Layout.createSequentialGroup()
                                    .addComponent(jLabel4)
                                    .addGap(48, 48, 48))
                                .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jPanel1Layout.createSequentialGroup()
                                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                        .addComponent(jLabel5, javax.swing.GroupLayout.Alignment.LEADING)
                                        .addComponent(jLabel6, javax.swing.GroupLayout.Alignment.LEADING)
                                        .addComponent(jLabel12, javax.swing.GroupLayout.Alignment.LEADING))
                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)))
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addComponent(jLabel1)
                                .addGap(68, 68, 68)))
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(service_num, javax.swing.GroupLayout.PREFERRED_SIZE, 260, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jScrollPane4, javax.swing.GroupLayout.DEFAULT_SIZE, 262, Short.MAX_VALUE)
                            .addComponent(service_person, javax.swing.GroupLayout.DEFAULT_SIZE, 262, Short.MAX_VALUE)
                            .addComponent(equipment, javax.swing.GroupLayout.PREFERRED_SIZE, 260, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(dateText, javax.swing.GroupLayout.PREFERRED_SIZE, 260, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(call_type, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(part_changed, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
                .addGap(44, 44, 44)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel2)
                    .addComponent(jLabel7)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(77, 77, 77)
                        .addComponent(jLabel3))
                    .addComponent(jLabel11)
                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                        .addComponent(completion_time, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(breakdown_time, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 262, Short.MAX_VALUE))
                    .addComponent(jScrollPane3, javax.swing.GroupLayout.PREFERRED_SIZE, 262, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel10)
                    .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 262, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addContainerGap(438, Short.MAX_VALUE)
                .addComponent(btnSubmit)
                .addGap(301, 301, 301))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(23, 23, 23)
                        .addComponent(jLabel1))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(service_num, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(16, 16, 16))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(jLabel7)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(breakdown_time, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(2, 2, 2)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(dateText, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel8)
                    .addComponent(jLabel10, javax.swing.GroupLayout.PREFERRED_SIZE, 22, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(2, 2, 2)
                .addComponent(completion_time, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel3)
                .addGap(1, 1, 1)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(equipment, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel9)
                    .addComponent(jLabel11))
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                        .addGap(24, 24, 24)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(service_person, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel4))
                        .addGap(18, 18, 18)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel5)
                            .addComponent(call_type, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(18, 18, 18)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel6)
                            .addComponent(part_changed, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(18, 18, 18)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jScrollPane4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel12)))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jLabel2)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jScrollPane3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(29, 29, 29)
                .addComponent(btnSubmit, javax.swing.GroupLayout.PREFERRED_SIZE, 36, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        add(jPanel1);
        jPanel1.setBounds(10, 57, 810, 438);

        lblHeader.setFont(new java.awt.Font("Times New Roman", 1, 24)); // NOI18N
        lblHeader.setText("jLabel14");
        add(lblHeader);
        lblHeader.setBounds(199, 11, 545, 28);
    }// </editor-fold>//GEN-END:initComponents

    private void btnSubmitActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSubmitActionPerformed
        // TODO add your handling code here:
        
        //String date = df.format( warranty_start_date.getDate());
        if(service_num.getText().isEmpty()||dateText.getDate().toString().isEmpty())
        {
           
            JOptionPane.showMessageDialog(null, "Required * fields cannot be empty.");
        }
        else
        {
            String msg=""; int err = 0;
           
            
            if(err!=1)
            {
                    Map<String, String> forminputs = new HashMap<>();

                 forminputs.put("service_num", service_num.getText());
                 forminputs.put("date", df.format( dateText.getDate()));

                 String equip = equipment.getSelectedItem().toString().trim();
                 Map<String, String> getId = modalEquipmentsObject.selectMapped("serial_no='"+equip+"'");
                 forminputs.put("equipment_id",getId.get("id"));

                 //forminputs.put("equipment", equipment.getSelectedItem().toString());
                 forminputs.put("complaint", complaintText.getText());
                 forminputs.put("action_taken", action_taken.getText());
                 forminputs.put("service_person", service_person.getText());
                 forminputs.put("call_type", call_type.getSelectedItem().toString());
                 forminputs.put("part_changed", part_changed.getSelectedItem().toString());
                 forminputs.put("breakdown_time", df.format( breakdown_time.getDate()));
                 forminputs.put("completion_time", df.format( completion_time.getDate()));
                 forminputs.put("remarks", remarks.getText());


                 if(this.forminfo.get("id")== null)
                 {
                        if(modalObject.selectCount("service_num='"+service_num.getText()+"'") > 0)
                        {
                            msg +=",Service number already exists.";
                            err =1;
                        }
                        
                        if(err!=1)
                        {
                            forminputs.put("created_by",forminfo.get("user_name"));
                            forminputs.put("created_at",df.format(date));
                            forminputs.put("updated_by",forminfo.get("user_name"));
                            forminputs.put("updated_at",df.format(date));
                            this.modalObject.insertData(forminputs);

                            JOptionPane.showMessageDialog(null, "Successfully Added Service Logs");
                        }
                        else
                        {
                             JOptionPane.showMessageDialog(null,msg);
                        }
                  
                 }
                 else
                 {
                     if(modalObject.selectCount("service_num='"+service_num.getText()+"'AND id<>'"+this.forminfo.get("id")+"'") > 0)
                        {
                            msg +=",Service number already exists.";
                            err =1;
                        }
                     if(err!=1)
                     {
                        forminputs.put("updated_by",forminfo.get("user_name"));
                            forminputs.put("updated_at",df.format(date));
                          String id = this.forminfo.get("id");
                          this.modalObject.updateData(forminputs, id);
                          JOptionPane.showMessageDialog(null, "Successfully Updated"+service_num.getText()); 
                     }
                     else
                     {
                       JOptionPane.showMessageDialog(null,msg);  
                     }
                     
                 } 
            }
            else
            {
                 JOptionPane.showMessageDialog(null,msg);
            }
            
            
          
            
        }
        
        
        //this.exit();
    }//GEN-LAST:event_btnSubmitActionPerformed

    private void service_personActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_service_personActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_service_personActionPerformed
   


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JTextArea action_taken;
    private org.jdesktop.swingx.plaf.basic.BasicDatePickerUI basicDatePickerUI1;
    private org.jdesktop.swingx.plaf.basic.BasicDatePickerUI basicDatePickerUI2;
    private org.jdesktop.swingx.JXDatePicker breakdown_time;
    private javax.swing.JButton btnSubmit;
    private org.jdesktop.swingx.JXComboBox call_type;
    private javax.swing.JTextArea complaint;
    private javax.swing.JTextArea complaintText;
    private org.jdesktop.swingx.JXDatePicker completion_time;
    private org.jdesktop.swingx.JXDatePicker dateText;
    private org.jdesktop.swingx.JXComboBox equipment;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JScrollPane jScrollPane4;
    private javax.swing.JLabel lblFormHeader;
    private javax.swing.JLabel lblHeader;
    private org.jdesktop.swingx.JXComboBox part_changed;
    private javax.swing.JTextArea remarks;
    private javax.swing.JTextField service_num;
    private javax.swing.JTextField service_person;
    // End of variables declaration//GEN-END:variables
}
