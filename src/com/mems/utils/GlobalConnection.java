/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mems.utils;
import java.sql.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.*;

/**
 *
 * @author Pukar
 */
public class GlobalConnection {
    public static Connection cn = null;
    

    public static void PerformConnection(){
        try{
            Class.forName("com.mysql.jdbc.Driver");
            cn = DriverManager.getConnection("jdbc:mysql://localhost/db_medidb","root","123123");
        }
        catch(Exception ex){
            JOptionPane.showMessageDialog(null, ex);
        }
    }
    
    public static void closeConnection()
    {
        try {
            cn.close();
        } catch (SQLException ex) {
            Logger.getLogger(GlobalConnection.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
}
