/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mems.utils;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 *
 * @author Pukar
 */
public class ValidationHelper {
    
    public static boolean isValidEmail(String email) {
        String ePattern = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"+ "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
        Pattern p = Pattern.compile(ePattern);
        Matcher m = p.matcher(email);
        return m.matches();
    }
    
    public static boolean isValidNumber(String number) {
    try {
      Integer.parseInt(number);
      return true;
    } catch (NumberFormatException e) {
      return false;
    }
  }
    
     public static boolean isValidPositiveNumber(String number) {
    try {
      if(Integer.parseInt(number)>0)
        return true;
      return false;
    } catch (NumberFormatException e) {
      return false;
    }
  }
}
