/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mems.utils;

/**
 *
 * @author Pukar
 */
public class RoleHelper {
    
    private static String userRole;
    
    private static String userName;

    public String getUserRole() {
        return userRole;
    }

    public void setUserRole(String userRole) {
        RoleHelper.userRole = userRole;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        RoleHelper.userName = userName;
    }
    
    public boolean isAdmin()
    {
        return "Admin".equals(this.getUserRole());
    }
    
   
    
    
    
}
