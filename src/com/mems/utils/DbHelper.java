/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mems.utils;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.swing.JOptionPane;
import net.proteanit.sql.DbUtils;

/**
 *
 * @author Pukar
 */
public class DbHelper {
    PreparedStatement ps;
    ResultSet rs=null;

    /**
     *  Requires the name of the table
     * @param tblName
     */
    public DbHelper(String tblName) {
       this.dbTable = tblName;
    }
  private final String dbTable;
  
    /**
     *  provide key as column name and value as the new value to be updated as a map and
     * id of the row to update
     * @param m
     * @param id
     * @return boolean
     */
    public boolean updateData(Map m,String id)
    {
        
        String convertedString = m.toString().replace("{", "").replace("}", "").replace("=", "='").replace(",", "',").trim()+"'";
        String query = "update "+dbTable+" set "+convertedString + " where id='"+id+"'";
        try{
            GlobalConnection.PerformConnection();
            ps=GlobalConnection.cn.prepareStatement(query);
            ps.executeUpdate();
            GlobalConnection.closeConnection();
            return true;
            
        }
        catch (Exception ex) {
            JOptionPane.showMessageDialog(null, ex);
        }
        return false;
    }
  
    /**
     *  provide key as column name and value as the new value to be saved as a map.
     * @param m
     * @return boolean
     */
    public boolean insertData(Map m)
  {
        String buildKey = "";
        String buildVal = "";
        for ( Object key : m.keySet() ) {
           buildKey = buildKey + "," + key.toString();     
        }
        buildKey = buildKey.substring(1);
        
        for ( Object vall : m.values()) {
           buildVal = buildVal + "','" + vall.toString();    
        }
        buildKey = buildKey.substring(0);
        buildVal = buildVal.substring(2)+"'";
        
        String query = "insert into "+dbTable+" ("+buildKey+") values ("+buildVal+")";
        try{
            GlobalConnection.PerformConnection();
            ps=GlobalConnection.cn.prepareStatement(query);
            ps.execute();
            GlobalConnection.closeConnection();
            return true;
            
        }
        catch (Exception ex) {
            JOptionPane.showMessageDialog(null, ex+buildKey);
        }
        return false;
  }
  
    /**
     *  id of row to delete.
     * @param id
     * @return
     */
    public boolean deleteData(String id)
  {
      String query = "DELETE FROM "+dbTable+" WHERE id = "+id;
      try{
            GlobalConnection.PerformConnection();
            ps=GlobalConnection.cn.prepareStatement(query);
            ps.executeUpdate();
            GlobalConnection.closeConnection();
            return true;
            
        }
        catch (Exception ex) {
            JOptionPane.showMessageDialog(null, ex);
        }
        return false;
  }
  
    /**
     * Returns data as object suitable for jtable. first parameter accepts column selections
     * and second parameter is where condition in sql query.
     * eg: selectModal('id,student_name AS First Name,address','course_id=5')
     * @param selection
     * @param condition
     * @return
     */
    public Object selectModel(String selection,String condition){
        
        Object tblmodel = new Object();
        String query;
        if(condition != null)
            query="select "+selection+" from "+dbTable+" where "+condition;
        else
            query="select "+selection+" from "+dbTable+"";
        try{
            GlobalConnection.PerformConnection();
            
            ps = GlobalConnection.cn.prepareStatement(query);
              rs = ps.executeQuery();
               tblmodel = DbUtils.resultSetToTableModel(rs);
               GlobalConnection.closeConnection();
             
        } catch (Exception ex) {
            JOptionPane.showMessageDialog(null, ex);
        }
        return tblmodel;
    }
    
    
    
    /**
     * Returns data as object suitable for jtable.accepts column selections
     * eg: selectModal('id,student_name AS First Name,address')
     * @param selection
     * @return object
     */
    public Object selectModel(String selection){
        
        return selectModel(selection,null);
    }
    
    /**
     * Returns data as nested list. first parameter accepts column selections
     * and second parameter is where condition in sql query.
     * eg: selectList('id,student_name AS First Name,address','course_id=5')
     * @param selection
     * @param condition
     * @return
     */
    public List selectList(String selection,String condition){
        
        Object tblmodel = new Object();
        String query;
        if(condition != null)
            query="select "+selection+" from "+dbTable+" where "+condition;
        else
            query="select "+selection+" from "+dbTable+"";
        try{
            GlobalConnection.PerformConnection();
            
            ps = GlobalConnection.cn.prepareStatement(query);
              rs = ps.executeQuery();
               tblmodel = DbUtils.resultSetToNestedList(rs);
               GlobalConnection.closeConnection();
             
        } catch (Exception ex) {
            JOptionPane.showMessageDialog(null, ex);
        }
        return (List) tblmodel;
    }
    
    /**
     * Returns data as nested list.accepts column selections
     * eg: selectList('id,student_name AS First Name,address')
     * @param selection
     * @return object
     */
    public List selectList(String selection){
        
        return selectList(selection,null);
    }
    
    /**
     * Returns single row data as a map. first parameter accepts column selections
     * and second parameter is where condition in sql query.
     * eg: selectModal('id,student_name AS First Name,address','course_id=5')
     * @param condition
     * @return
     */
    public Map<String, String> selectMapped(String condition){
        
       Map<String, String> row = new HashMap<>();
        String query;
        if(condition != null)
            query="select * from "+dbTable+" where "+condition+" limit 1";
        else
            query="select * from "+dbTable+" limit 1";
        try{
            GlobalConnection.PerformConnection();
            
            ps = GlobalConnection.cn.prepareStatement(query);
              rs = ps.executeQuery();
              ResultSetMetaData md = rs.getMetaData();
               int columns = md.getColumnCount();
               while (rs.next()) {
                   
            for (int i = 1; i <= columns; ++i) {
                    String val = null;
                    if (rs.getObject(i) != null)
                    val = rs.getObject(i).toString().trim();
                
                String key = md.getColumnName(i).trim();
                row.put(key, val);
            }
        }
               GlobalConnection.closeConnection();
             
        } catch (Exception ex) {
            JOptionPane.showMessageDialog(null, ex);
        }
        return row;
    }
    
    
   public Map<String, String> selectListMapped(String selection,String condition){
        Map<String, String> row = new HashMap<>();
        String query;
        if(condition != null)
            query="select "+selection+" from "+dbTable+" where "+condition+""; 
           
        else
            query="select "+selection+" from "+dbTable+"";
        try{
            GlobalConnection.PerformConnection();
            
            ps = GlobalConnection.cn.prepareStatement(query);
              rs = ps.executeQuery();
              ResultSetMetaData md = rs.getMetaData();
               int columns = md.getColumnCount();
              String val=null,key=null;
        while (rs.next()) {
            
            for (int i = 1; i < columns; ++i) {
                
                if (rs.getObject(i) != null)
                    val = rs.getObject(i).toString().trim();
                if (rs.getObject(i+1) != null)
                 key = rs.getObject(i+1).toString().trim();
                row.put(key, val);
            }
            //row.put(key, val);
        }
        }catch (Exception ex) {
            JOptionPane.showMessageDialog(null, ex);
        }
      return row;  
    }
    
    
     public Map<String, String> selectListMapped(String selection){
         return selectListMapped(selection,null);
     }
    
   
    /**
     * Returns row count as int.accepts where condition 
     * eg: selectCount('course_id=5')
     * @param condition
     * @return int
     */
    public int selectCount(String condition){
        
        String query;
        int count=0;
        if(condition != null)
            query="select count(*) from "+dbTable+" where "+condition+"";
        else
            query="select count(*) from "+dbTable+"";
         try{
            GlobalConnection.PerformConnection();
            ps=GlobalConnection.cn.prepareStatement(query);
            rs = ps.executeQuery();
             if(rs.next()){
                 count = rs.getInt("count(*)");
             }
            GlobalConnection.closeConnection();
            
        }
        catch (Exception ex) {
            JOptionPane.showMessageDialog(null, ex);
        }
        return count;
    }
  
  
}
