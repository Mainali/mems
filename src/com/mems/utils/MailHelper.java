/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mems.utils;

import java.util.Date;
import java.util.Map;
import java.util.Properties;

import javax.mail.Message;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.swing.JOptionPane;
/**
 *
 * @author Pukar
 */
public class MailHelper {

    public MailHelper() {
        
    }
    
    DbHelper dbobject = new DbHelper("system_configs");
    
//    public static void main(String [] args)
//{
public void sendMail(String to,String subject,String Messagebody)
{
    String Smtphost = "smtp.mandrillapp.com";
    String Smtpport = "587";
    String emails = "mainalipukar@gmail.com";//your email
    String pass = "sdfsdfsdfsdf "; //your password
    
    Map<String,String> configdata = dbobject.selectMapped(null);
     Smtphost = configdata.get("mail_smtp_host");
     Smtpport = configdata.get("mail_smtp_port");
     emails =    configdata.get("email");
     pass =   configdata.get("mail_auth_code");
    
    String tosend = to;
    String subjectsend = subject;
    String messages = Messagebody;
    
    try{

        Properties props = new Properties();
        props.put("mail.smtp.host", Smtphost); // for gmail use smtp.gmail.com
        props.put("mail.smtp.auth", "true");
        props.put("mail.debug", "true"); 
        props.put("mail.smtp.starttls.enable", "true");
        props.put("mail.smtp.port", Smtpport);
        props.put("mail.smtp.socketFactory.port", "465");
        props.put("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
        props.put("mail.smtp.socketFactory.fallback", "false");

        Session mailSession;
        mailSession = Session.getInstance(props, new javax.mail.Authenticator() {
           String emails =  configdata.get("email");
           String pass =   configdata.get("mail_auth_code");
            protected PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication(emails, pass);
            }
        });

        mailSession.setDebug(true); // Enable the debug mode

        Message msg = new MimeMessage( mailSession );

        //--[ Set the FROM, TO, DATE and SUBJECT fields
        msg.setFrom( new InternetAddress( emails ) );
        msg.setRecipients( Message.RecipientType.TO,InternetAddress.parse(tosend) );
        msg.setSentDate( new Date());
        msg.setSubject( subjectsend );

        //--[ Create the body of the mail
        msg.setText( messages );

        //--[ Ask the Transport class to send our mail message
        Transport.send( msg );

    }catch(Exception E){
       
         JOptionPane.showMessageDialog(null, E);
    }
}
}
