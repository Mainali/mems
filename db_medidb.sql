-- phpMyAdmin SQL Dump
-- version 4.0.10deb1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Jan 20, 2016 at 05:37 PM
-- Server version: 5.5.41-0ubuntu0.14.04.1
-- PHP Version: 5.5.9-1ubuntu4.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `db_medidb`
--

-- --------------------------------------------------------

--
-- Table structure for table `departments`
--

CREATE TABLE IF NOT EXISTS `departments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `code_name` varchar(255) NOT NULL,
  `incharge` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `equipments`
--

CREATE TABLE IF NOT EXISTS `equipments` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `department` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `supplier` int(11) NOT NULL,
  `general_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `full_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `code` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `manufacturer` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `model_no` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `serial_no` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `installation_date` date NOT NULL,
  `warranty_type` enum('AMC','CMC','other','none') COLLATE utf8_unicode_ci NOT NULL,
  `category` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `cost` double NOT NULL,
  `warranty_start_date` date NOT NULL,
  `warranty_end_date` date NOT NULL,
  `notified` tinyint(1) NOT NULL DEFAULT '0',
  `created_by` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `updated_by` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=6 ;

--
-- Dumping data for table `equipments`
--

INSERT INTO `equipments` (`id`, `department`, `supplier`, `general_name`, `full_name`, `code`, `manufacturer`, `model_no`, `serial_no`, `installation_date`, `warranty_type`, `category`, `cost`, `warranty_start_date`, `warranty_end_date`, `notified`, `created_by`, `updated_by`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, '1', 1, 'Baby warmer', 'Lullaby', 'OM20', 'GE Healthcare', 'LBY23', 'b20WHJ', '2015-06-10', 'AMC', 'life_care', 600000, '2015-11-30', '2015-11-30', 0, '', 'Mainali', '2015-06-10 08:52:15', '2016-01-19 18:15:00', NULL),
(2, '1', 1, 'Baby Incubator', 'Lullaby', 'OM23', 'GE Healthcare', 'LBY256', 'lb7678', '2015-06-11', 'AMC', 'life_care', 600000, '2015-06-11', '2016-06-15', 0, '', 'Mainali', '2015-06-10 20:26:54', '2016-01-19 18:15:00', NULL),
(3, '3', 2, 'Ventilator Ge', 'Engistrom Carestation', 'Nor13', 'GE Healthcare', 'GH345', '1134EWXV98M9', '2015-06-11', 'AMC', 'life_care', 2400000, '2015-06-11', '2016-06-11', 0, '', 'Mainali', '2015-06-10 21:05:13', '2016-01-19 18:15:00', NULL),
(4, '3', 2, 'Humidifier', 'Humidifying unit', 'Nor14', 'GE Healthcare', 'xl67', 'xls12', '2015-06-11', 'none', 'life_care', 600000, '0001-01-01', '0001-01-01', 0, '', 'Mainali', '2015-06-10 21:06:41', '2016-01-19 18:15:00', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `service_logs`
--

CREATE TABLE IF NOT EXISTS `service_logs` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `service_num` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `date` date NOT NULL,
  `equipment_id` int(11) NOT NULL,
  `complaint` text COLLATE utf8_unicode_ci NOT NULL,
  `action_taken` text COLLATE utf8_unicode_ci NOT NULL,
  `service_person` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `call_type` enum('technical','operational','preventive','others') COLLATE utf8_unicode_ci NOT NULL,
  `part_changed` enum('no','yes') COLLATE utf8_unicode_ci NOT NULL,
  `breakdown_time` datetime NOT NULL,
  `completion_time` datetime NOT NULL,
  `remarks` text COLLATE utf8_unicode_ci NOT NULL,
  `updated_by` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_by` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=7 ;

--
-- Dumping data for table `service_logs`
--

INSERT INTO `service_logs` (`id`, `service_num`, `date`, `equipment_id`, `complaint`, `action_taken`, `service_person`, `call_type`, `part_changed`, `breakdown_time`, `completion_time`, `remarks`, `updated_by`, `created_by`, `deleted_at`, `created_at`, `updated_at`) VALUES
(2, '55665', '2015-09-27', 3, 'what the fuck', 'circuit connected properly and re run the checkout and it passed.', 'pukar', 'operational', 'no', '2015-09-27 00:00:00', '2015-09-27 00:00:00', 'done', 'Mainali', '', NULL, '2015-09-27 11:39:14', '2016-01-19 18:15:00');

-- --------------------------------------------------------

--
-- Table structure for table `suppliers`
--

CREATE TABLE IF NOT EXISTS `suppliers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(250) NOT NULL,
  `address` varchar(250) NOT NULL,
  `phone_no` varchar(250) NOT NULL,
  `email` varchar(250) NOT NULL,
  `created_by` varchar(250) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `updated_by` varchar(250) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `suppliers`
--

INSERT INTO `suppliers` (`id`, `name`, `address`, `phone_no`, `email`, `created_by`, `created_at`, `updated_at`, `updated_by`) VALUES
(1, 'Pukar', 'kalopool', '9841052762', 'pukar@gmail.com', 'Mainali', '2016-01-18 00:00:00', '2016-01-18 00:00:00', 'Mainali'),
(2, 'Bemita', 'Gwarkoqwqw', '984561526', 'bemita@gmail.com', 'Mainali', '2016-01-18 00:00:00', '2016-01-20 00:00:00', 'Mainali');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `role` enum('Guest','Admin') NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `username`, `password`, `email`, `role`) VALUES
(1, 'Pukar', 'pukar123', '', 'Guest'),
(2, 'Mainali', 'pukar123', 'mainalipukar@gmail.com', 'Admin');

-- --------------------------------------------------------

--
-- Table structure for table `work_logs`
--

CREATE TABLE IF NOT EXISTS `work_logs` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `date` date NOT NULL,
  `work_done` text COLLATE utf8_unicode_ci NOT NULL,
  `remarks` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_by` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `updated_by` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=9 ;

--
-- Dumping data for table `work_logs`
--

INSERT INTO `work_logs` (`id`, `user`, `date`, `work_done`, `remarks`, `created_at`, `updated_at`, `created_by`, `updated_by`) VALUES
(1, 'Mainali', '2015-06-09', 'some critical work..', 'well done', '2015-06-08 22:49:15', '2016-01-19 18:15:00', '', 'Mainali'),
(2, 'Mainali', '2015-06-09', 'different', 'ok', '2016-01-11 18:15:00', '2016-01-19 18:15:00', '', 'Mainali'),
(4, 'Pukar', '2015-06-13', 'Tread mill issue was solved at Norvic Hospital.', 'done.asdasd', '2015-06-13 11:38:38', '2016-01-19 18:15:00', '', 'Mainali');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
